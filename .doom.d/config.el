;;; $DOOMDIR/ -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
 (setq doom-font (font-spec :family "monospace" :size 18 :weight 'semi-light)
       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-monokai-classic)
;; (setq doom-theme 'doom-dracula)
;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; MY CUSTOM SETTINGSE
(setq dashboard-banner-logo-title "“Nothing is yours. It is to use. It is to share. If you will not share it, you cannot use it.”")

;; GLOVAACS
(global-set-key (kbd "C-S-t") 'treemacs)
(global-set-key (kbd "M-t") 'treemacs-select-window)
(global-set-key [M-left] 'centaur-tabs-backward)
(global-set-key [M-right] 'centaur-tabs-forward)

(global-set-key (kbd "C-S-p") 'projectile-find-file)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region )
(global-set-key (kbd "C-S-i") 'prettier-js-mode)
(global-set-key (kbd "C-S-f") 'helm-ag-project-root)
(global-set-key (kbd "C-c C-t") 'hs-toggle-hiding)
(global-set-key (kbd "C-x C-b") 'ibuffer)
;;(global-set-key (kbd "C-S-w") 'web-mode)
(global-set-key (kbd "C-x C-l") 'comment-or-uncomment-region)
(global-set-key (kbd "C-x C-g C-m") 'vc-msg-show) ;; git message, "git blame"
(global-set-key (kbd "C-x TAB") 'indent-rigidly)
(global-set-key (kbd "C-j") 'emmet-expand-line)

(windmove-default-keybindings)

(treemacs-resize-icons 20)

;; CENTAUR TABS
(setq centaur-tabs-style "wave")
(setq centaur-tabs-set-bar 'under)
(setq x-underline-at-descent-line t)
(setq centaur-tabs-cycle-scope 'tabs) ;; cycle through visible tabs only
;;(centaur-tabs-group-by-projectile-project)
;;(centaur-tabs-change-fonts "arial" 160)
(setq centaur-tabs-height 52)
;;(centaur-tabs-headline-match)


;; UNDO REDO TREE
(global-undo-tree-mode 1)
(global-set-key (kbd "C-/") 'undo)
(defalias 'redo 'undo-tree-redo)
(global-set-key (kbd "C-?") 'redo)


;; EMACSSHOT
;; create code screenshot with mouse dragging
;;
(global-set-key (kbd "C-c s h") 'emacsshot-snap-mouse-drag)
;;
;;


;; OPEN SHELL FRAME
(global-set-key (kbd "C-c C-1") 'shell)

(setq lsp-treemacs-sync-mode 1)

(add-hook 'vue-mode-hook 'emmet-mode)
(add-hook 'vue-mode-hook 'prettier-js-mode)
;; (add-hook 'vue-mode-hook 'web-mode)
(add-hook 'vue-mode-hook #'lsp!)
(add-to-list 'auto-mode-alist '("\\.vue\\'" . vue-mode))
;; (use-package! tree-sitter
;;   :config
;;   (require 'tree-sitter-langs)
;;   (global-tree-sitter-mode)
;;   (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))


;; for completions
(use-package company-lsp
  :after lsp-mode
  :config (push 'company-lsp company-backends))



(use-package multiple-cursors
  :bind (("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ("C-c C->" . mc/mark-more-like-this-extended))
  :init
 )

;; set custom doom dashboard image
(setq fancy-splash-image "/home/kobi/Pictures/blame-doom-2.jpg")

(use-package lsp-ui)

(global-set-key (kbd "C-f") 'lsp-ui-doc-focus-frame)
(global-set-key (kbd "C-u") 'lsp-ui-doc-unfocus-frame)


(add-hook 'prog-mode-hook 'lsp-ui-doc-mode)

(nyan-mode)
;; (nyan-start-animation)
(setq nyan-wavy-trail t)

(setq lsp-ui-doc-max-width 350)
;; show errors on-the-fly, not after save
(after! flycheck
  (add-to-list 'flycheck-check-syntax-automatically 'idle-change))

;; change active window size
;; (custom-set-variables '(zoom-mode t))
;; (setq golden-ratio-mode 1)
;; (use-package golden-ratio
;;   :ensure t
;;   :diminish golden-ratio-mode
;;   :init
;;   (golden-ratio-mode 1))


;;Anaconda support
;;(require 'conda)

;; (setq conda-env-home-directory "<path-to>/anaconda3")
;; (conda-env-activate 'getenv "CONDA_DEFAULT_ENV")
;; (setq-default mode-line-format (cons mode-line-format '(:exec conda-env-current-name)))
